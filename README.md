# Qwantsearch


This module integrates the [Qwant](https://www.qwant.com) search engine in
Drupal.

For now, you are able to:
- Configure a single search page url
- Configure the number of search results you want to display


## Requirements

This module requires the following modules:
- [Imagecache external](https://www.drupal.org/project/imagecache_external)

This module requires the
[PHP cURL library](https://www.drupal.org/requirements/php/curl).

Your site must be able to contact Qwant servers on port 80 to work properly.

You must obtain a Qwant partner ID and HTTP Token. Contact
[Qwant](https://www.qwant.com) directly.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the Qwantsearch module on your main site.
1. Go to the configuration page (`admin/config/search/qwantsearch`) and choose
   your settings.
1. Go to the block administration page (`admin/structure/block`) and place the
   Qwantsearch block on your site.
1. Go to the front and start a search using the search block.


## Maintainers

Current maintainers:
- Bastien Rigon - [barig](https://www.drupal.org/user/2537604)

This project has been sponsored by:
- [Smile](https://www.drupal.org/smile)
