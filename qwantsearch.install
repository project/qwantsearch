<?php

/**
 * @file
 * Install, update and uninstall functions for the Qwantsearch module.
 */

declare(strict_types = 1);

/**
 * Implements hook_requirements().
 */
function qwantsearch_requirements(string $phase): array {
  $requirements = [];

  $has_curl = \function_exists('curl_init');

  $requirements['curl'] = [
    'title' => \t('cURL'),
    'value' => $has_curl ? \t('Enabled') : \t('Not found'),
  ];
  if (!$has_curl) {
    $requirements['curl']['severity'] = REQUIREMENT_ERROR;
    $requirements['curl']['description'] = \t('The Qwantsearch module requires the <a href="https://secure.php.net/manual/en/curl.setup.php">PHP cURL library</a>. For more information, see the <a href="https://www.drupal.org/requirements/php/curl">online information on installing the PHP cURL extension</a>.');
  }

  return $requirements;
}
