<?php

declare(strict_types = 1);

namespace Drupal\qwantsearch\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines dynamic routes.
 */
class SearchPageRouting {

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * SearchPageRouting constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * Create the search page dynamically.
   *
   * @return array
   *   The route to display the search page.
   */
  public function routes(): array {
    $routes = [];
    /** @var string $qwantsearch_search_page */
    $qwantsearch_search_page = $this->configFactory->get('qwantsearch.settings')->get('qwantsearch_search_page');
    $routes['qwantsearch.search_page'] = new Route(
      '/' . \trim($qwantsearch_search_page, '/ '),
      [
        '_controller' => '\Drupal\qwantsearch\Controller\QwantSearchController::getSearchPageContent',
        '_title_callback' => '\Drupal\qwantsearch\Controller\QwantSearchController::getSearchPageTitle',
      ],
      [
        '_permission' => 'access_qwant_search_page',
      ]
    );
    return $routes;
  }

}
